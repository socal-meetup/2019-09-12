# Orange County GitLab Users Meetup
## 2019-09-12

### Topics
Come meet with other DevOps professionals and get tips on how to build an awesome CI/CD Pipeline. 

### Agenda
1.  New CI/CD Features in GitLab
1.  Using AutoDevOps to get your app to the cloud.
2.  gitlab runners
3.  container registry


### Examples
1.  https://gitlab.com/bdowney/aspmvc
1.  https://gitlab.com/bdowney/terraform-demo


### Documentation Links
1.  https://docs.gitlab.com/ee/topics/autodevops/
2.  https://docs.gitlab.com/ee/ci/yaml/
3.  https://docs.gitlab.com/ee/user/project/clusters/


### Notes
git submoudules -- https://www.youtube.com/watch?v=UQvXst5I41I

